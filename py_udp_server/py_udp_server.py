import rclpy
from rclpy.node import Node
import socket
import struct
from rclpy.executors import SingleThreadedExecutor

from std_msgs.msg import String
from voltron_msgs.msg import WheelSpeedMsg 

SERVER_IP = "0.0.0.0"
SERVER_PORT = 7777
VCU_IP = "10.0.0.3"
VCU_PORT = 8888

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(("0.0.0.0",SERVER_PORT))

status_bytes = struct.pack('<fff', 1.0, 2.0, 3.0)

# rclpy.__init__('udp_server')
# 
# rclpy.init()
# rclpy.publisher_ = rclpy.create_publisher(String, 'topic', 10)
# 



class MinimalPublisher(Node):

    def __init__(self):
        super().__init__('minimal_publisher')

        self.publisher_ = self.create_publisher(WheelSpeedMsg, 'topic', 10)

    def publish_rpm(self, rpm_tuple):
        fl_rpm = rpm_tuple[1]
        fr_rpm = rpm_tuple[0]
        msg = WheelSpeedMsg()
        msg.fl_wheel_rpm = fl_rpm
        msg.fr_wheel_rpm = fr_rpm
        msg.header.stamp = self.get_clock().now().to_msg()
        self.publisher_.publish(msg)


def main(args=None):
    rclpy.init(args=args)

    executor = SingleThreadedExecutor()
    minimal_publisher = MinimalPublisher()
    executor.add_node(minimal_publisher)
    counter = 0
    while rclpy.ok():
        executor.spin_once(0)
        status_bytes, addr = sock.recvfrom(1024)
        if (len(status_bytes) > 0):
            status_data = struct.unpack('<ff', status_bytes)    
            #print(f'{counter} Left Wheel rpm: {status_data[0]}, Right Wheel rpm: {status_data[1]},')
            counter += 1
            minimal_publisher.publish_rpm(status_data)



    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
    
